<?php
	session_start();
	if(!isset($_SESSION['zalogowany']))
	{
		header('Location:index.php');
		exit();
		
	}
	$e_haslo='';
	//laczenie z baza danych
	require_once "connect.php";
	$polaczenie= @new mysqli($host,$db_user,$db_password, $db_name);
	
	if($polaczenie->connect_errno!=0)
	{
		echo "Error:".$polaczenie->connect_errno;
		exit;
	}
	
	//sprawdza czy jest ustawiona zmienna haslo1
	if(isset($_POST['haslo1']))
	{
		$haslo1=@$_POST['haslo1'];
		
		// Sprawdź, czy użytkownik o podanym  haśle isnieje w bazie danych
		$sql=('SELECT pass FROM uzytkownicy WHERE id = "'.$_SESSION['id'].'"');
		if ($rezultat= @$polaczenie->query($sql))
		{
			$wiersz= $rezultat-> fetch_assoc();
			$aktualnehaslo= $wiersz['pass'];
		}
		
		//SPRAWDZANIE POPRAWNOSCI AKUALNEGO HASLA Z PODANYM HASLEM
		if (password_verify($haslo1,$aktualnehaslo))
		{
			//pobiera zawartosc pola "nowe haslo2"
			$haslo2=$_POST['haslo2'];
			$haslo3=$_POST['haslo3'];
			
			//sprawdz poprawność hasła
			if ((strlen($haslo2)<8) || (strlen($haslo2)>20))
			{
				$e_haslo="Hasło musi posiadać od 8 do 20 znaków!";		
			}	
			else if ($haslo2!=$haslo3)
			{
				$e_haslo="Podane hasła nie są identyczne!";	
			}
			else 
			{
				//ROBI AKTUALIZACJE DANYCH
				$haslo_hash= password_hash($haslo2,PASSWORD_DEFAULT);
				$sql= 'UPDATE uzytkownicy SET pass="'.$haslo_hash.'" WHERE id= "'.$_SESSION['id'].'"';
				if ($rezultat= @$polaczenie->query($sql))
				{
					$e_haslo="Hasło zostało zmienione!";
				}
			}
		}	
		else
		{
			$e_haslo="Aktualne hasło nie jest prawidłowe!";
		}
	}	
?>

<!DOCTYPE HTML>
<html lan="pl">
<head>
	<meta charset= "utf-8" />
	<meta http-equiv= "X-UA-Compatibile" content= "IE=edge,chrome=1"/>
	<title> Osadnicy - zmień swoje hasło! </title>
	<style>
	.error
	{
	color:red;
	margin-top: 10px;
	margin-bottom: 10px;
	}
	</style>
</head>
<body>
	<form method= "post" >
	<?php
	echo "<span style='color:Fuchsia'> Witaj ". $_SESSION['user']." w formularzu zmiany hasła: </span>"

	?>
	</br></br>
	Stare hasło: <br/><input type="password" name="haslo1"/><br/><br/>
	
	Nowe hasło: <br/><input type="password" name="haslo2"/><br/><br/>

	Powtórz nowe hasło : <br/><input type="password" name="haslo3"/><br/><br/>
	<?php
	echo $e_haslo;
	?>
	
	<br/>
	
	<br/><input type="submit" value="Zmień hasło"/><br/><br/>
	<a href="gra.php"> Powrót! </a>
	</form>

	
</body>

</html>
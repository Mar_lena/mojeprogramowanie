<?php
	session_start();
	
	if (isset($_POST['email']))
	{
		//udana walidacja? Załóży, że tak!
		$wszystko_OK= true;
		
		// Sprawdż poprawność nickname'a
		$nick= $_POST['nick']; 
		
		// Sprawdzenie długości nicka
		if ((strlen($nick)<3) || (strlen($nick)>20))
		{
			$wszystko_OK=false;
			$_SESSION['e_nick']="Nick musi posiadać od 3 do 20 znaków!";			
		}
		// Sprawdzenie polskich znaków w nicku
	if (ctype_alnum($nick)==false)
	{
		$wszystko_OK=false;
			$_SESSION['e_nick']="Nick może składać się tylko z liter i cyfr (bez polskich znaków)";
	
	}
		// Sprawdzenie poprawnosci adresu email
		$email= $_POST['email'];
		$emailB= filter_var($email,FILTER_SANITIZE_EMAIL);
		
		// sprawdza popawnosc meila
		if ((filter_var($emailB,FILTER_VALIDATE_EMAIL)==false) || ($emailB!=$email))
		{
			$wszystko_OK=false;
			$_SESSION['e_email']="Podaj poprawny adres e-mail";
			
		}

		//sprawdz poprawność hasła
		$haslo1=$_POST['haslo1'];
		$haslo2=$_POST['haslo2'];
		
		//sprawdzenie dlugosci hasla
			if ((strlen($haslo1)<8) || (strlen($haslo1)>20))
			{
				$wszystko_OK=false;
			$_SESSION['e_haslo']="Hasło musi posiadać od 8 do 20 znaków!";		
			}
			
		if ($haslo1!=$haslo2)
		{
				$wszystko_OK=false;
			$_SESSION['e_haslo']="Podane hasła nie są identyczne!";	
		}
			// HASHOWANIE HASEL
		$haslo_hash= password_hash($haslo1,PASSWORD_DEFAULT);
		
		// czy zaakceptowano regulamin
		if (!isset($_POST['regulamin']))
		{
		$wszystko_OK=false;
			$_SESSION['e_regulamin']="Potwierdź akceptację regulaminu!";	
		}
		
		//bot or not? oto jest pytanie. Sprawdzenie zaakceptowania captcha
		$sekret="6LctGRoTAAAAAMg1GlifREcK_GdwTWvis3jVhdf0";
		$sprawdz= file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$sekret.'&response='.$_POST['g-recaptcha-response']);
		
		$odpowiedz = json_decode($sprawdz);
		if ($odpowiedz-> success==false)
		{
			$wszystko_OK=false;
			$_SESSION['e_bot']="Potwierdź, że nie jesteś botem!";	
		}
		
		require_once"connect.php";
		mysqli_report(MYSQLI_REPORT_STRICT);
		
		
		try
		{
		$polaczenie= new mysqli($host,$db_user,$db_password, $db_name);
		if($polaczenie->connect_errno!=0)
		{
			throw new Exception(mysqli_connect_errno());
		}
		else
		{
			// czy email juz istnieje w bazie
			$rezultat= $polaczenie->query("SELECT id FROM uzytkownicy WHERE email='$email'");
			if (!$rezultat) throw new Exception($polaczenie-> error);
			
			$ile_takich_maili= $rezultat->num_rows;
			if($ile_takich_maili>0)
			{
				$wszystko_OK=false;
				$_SESSION['e_email']="Isnieje już konto przypisane do tego adresu e-mail!";
			}
			// czy nick juz istnieje w bazie
			$rezultat= $polaczenie->query("SELECT id FROM uzytkownicy WHERE user='$nick'");
			if (!$rezultat) throw new Exception($polaczenie-> error);
			
			$ile_takich_nickow= $rezultat->num_rows;
			if($ile_takich_nickow>0)
			{
				$wszystko_OK=false;
				$_SESSION['e_nick']="Isnieje już gracz o takim nicku. Wybierz inny!";
			}
			if ($wszystko_OK==true)
			{
				// Hurra, wszystkie testy zaliczone, dodajemy użytkownika do bazy!
			if ($polaczenie->query("INSERT INTO uzytkownicy VALUES (NULL,'$nick','$haslo_hash','$email',100,100,100,14,1)"))
			{
				$_SESSION['udanarejestracja']=true;
				header('Location: witamy.php');
				
			}
			
			else
			{
				throw new Exception($polaczenie-> error);
			}
			}
				$polaczenie->close();
			
		}
		}
		catch (Exception $e)
		{
				echo '<span style="color:red;"> Błąd serwera! przepraszamy za niedogodności i prosimy o 
				rejestrację w innym terminie!</span>';
				echo '<br/> Informacja developerska: '.$e;
			
		}
	}
?>

<!DOCTYPE HTML>
<html lan="pl">
<head>
	<meta charset= "utf-8" />
	<meta http-equiv= "X-UA-Compatibile" content= "IE=edge,chrome=1"/>
	<title> Osadnicy - załóż darmowe konto! </title>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	
	
	
	<style>
	.error
	{
	color:red;
	margin-top: 10px;
	margin-bottom: 10px;
	}
	</style>

</head>
<body>
	<form method= "post" >
	Nickname: <br/><input type="text" name="nick"/><br/>
	
	<?php
	if (isset($_SESSION['e_nick']))
	{
		echo '<div class= "error">'. $_SESSION['e_nick'].'</div>';
		unset($_SESSION['e_nick']);
	}
	?>
	
	E-mail: <br/><input type="text" name="email"/><br/>
	<?php
	if (isset($_SESSION['e_email']))
	{
		echo '<div class= "error">'. $_SESSION['e_email'].'</div>';
		unset($_SESSION['e_email']);
	}
	?>
	Twoje hasło: <br/><input type="password" name="haslo1"/><br/>
	
	<?php
	if (isset($_SESSION['e_haslo']))
	{
		echo '<div class= "error">'. $_SESSION['e_haslo'].'</div>';
		unset($_SESSION['e_haslo']);
	}
	?>
	Powtórz hasło: <br/><input type="password" name="haslo2"/><br/>
	<label>
	<input type="checkbox" name="regulamin"/> Akceptuje regulamin 
	</label>
	<?php
	if (isset($_SESSION['e_regulamin']))
	{
		echo '<div class= "error">'. $_SESSION['e_regulamin'].'</div>';
		unset($_SESSION['e_regulamin']);
	}
	?>
		

	<div class="g-recaptcha" data-sitekey="6LctGRoTAAAAAG6t_W3wBHIuuldhgaGgdBRwJSoa"></div>
		<?php
	if (isset($_SESSION['e_bot']))
	{
		echo '<div class= "error">'. $_SESSION['e_bot'].'</div>';
		unset($_SESSION['e_bot']);
	}
	?>
	
	<br/>
	
	<input type="submit" value="Zarejestruj się"/>
	
	
	
	</form>
	
</body>

</html>
	

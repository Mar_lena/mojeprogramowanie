class Account {
	constructor(){
		this.accountBalance = 0;
		this.accountHistory = [];
        this.description = "";
	}
	getAccountBalance(){
        return this.formatAccountBalance(); 
	}
	
	incomingPayment(money){
		money = parseInt(money);
		this.accountBalance = this.accountBalance + money;
        
        var historyRow = {};
        historyRow.money = money;
        var date = new Date()
        historyRow.date = (date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()+" " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds());
        
        var description = $('.incomingDescription').val();       
        historyRow.description = description;
        this.accountHistory.push(historyRow);
	}
	
	outgoingPayment(money){
        money = parseInt(money);
        if((this.accountBalance - money) >= 0){
            this.accountBalance = this.accountBalance - money;
            var historyRow = {};
            historyRow.money = -money;
            var date = new Date()
            historyRow.date = (date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()+" " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds());
            
            var description = $('.outgoingDescription').val();       
            historyRow.description = description;
            this.accountHistory.push(historyRow);
        } else {
            alert("Masz niewystarczającą ilość środków na swoim koncie!!!")
        }	
    }
	
	getHistory(){
        return this.accountHistory;
	}
    
    formatAccountBalance(){
        var accountBalanceStr = this.accountBalance.toString();
        var accountBalanceFomr = "";

        for( var i = accountBalanceStr.length - 1; i >= 0; i--){
          if((accountBalanceStr.length - i-1) % 3 === 0){
            accountBalanceFomr+= " ";
          }
            accountBalanceFomr = accountBalanceFomr + accountBalanceStr[i];
        }
        var accountBalanceFormat = accountBalanceFomr.split("").reverse().join("")
        return accountBalanceFormat;
           
    }
}
<?php
require_once"connect.php";
		mysqli_report(MYSQLI_REPORT_STRICT);
		mysql_connect($host, $db_user, $db_password,$db_name);
		mysql_select_db($db_name) or die (mysql_error());
		mysql_query("SET NAMES 'utf8'");
?>

<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset= "utf-8" />
	<meta http-equiv= "X-UA-Compatibile" content= "IE=edge,chrome=1"/>
	<link rel= "stylesheet" href= "style.css" type= "text/css" />
	<title> Wspinaczka sportowa w Polsce </title>
	<meta name="description" content="Storna poświęcona wspinaczce sportowej w Polsce. Wszystko co chciałbyś wiedzieć o wspinaczce. Moja przygoda ze wspinaniem sportowym.
	<meta name= "keywords" content= "wspinaczka, skały, wspinaczka skałowa, wspinaczka sportowa, wycena dróg, historia wspinania, wspinanie w Polsce, ścianka wspinaczkowa"/>
	<link href='https://fonts.googleapis.com/css?family=Freckle+Face|Halant:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<link  href= "css/fontello.css" rel= "stylesheet" type= "text/css" />
</head>

<body>
	<div id="container">
		<div id="logo"><a href="index.php" class="tilelink" >
			<img src="blondi.png" width=80px  high=80px style="float:left;"> 
			<span style="color:#c34f4f">Wspin</span>aczka  sportowa
		</a></div>
		<div id="menu">
			<ol>
				<li><a href="index.php">Strona główna </a></li>
				<li><a href="#">Historia wspinania  </a>
					<ul>
						<li><a href="index.php?id=8">Wspinaczka skałkowa </a></li>
						<li><a href="index.php?id=9">Wspinaczka ściankowa</a></li>
					</ul>
				</li>	
				<li><a href="#">Poradnik</a>
					<ul>
						<li><a href="#">Trening </a></li>
						<li><a href="#">Kontuzje </a></li>
						<li><a href="#">BHP wspinania </a></li>
					</ul>
				</li>
				<li><a href="#">Sprzęt  </a>
					<ul>
						<li><a href="#">Jakie buty wybrać? </a></li>
						<li><a href="#">Lina do wspinaczki</a></li>
						<li><a href="#">Sprzęt skałkowy </a></li>
						<li><a href="#">Przyrządy asekruacyjne </a></li>
					</ul>
				</li>
				<li><a href="#">Galeria</a>
					<ul>
						<li><a href="#">Wyjazdy skałkowe </a></li>
						<li><a href="#">Wspinanie ściankowe </a></li>
					</ul>
				</li>
				<li><a href="#">O mnie/Kontakt</a></li>
						
			</ol>

		</div>
			<div style="clear:both;"></div>
		<div id="topbar">
			<div id="topbarL">
				<img src="wspinaczka.jpg" width=120px  high=120px/>
				</div>
				<div id="topbarR">
					<span class="bigtitle">O wspinaczce słów kilka </span>
				<div style="height:15px;"></div>
					<i>"Wspinanie jest jak seks - nie musisz być dobry, aby się nim cieszyć"</i><br/><br/>
					Zapraszam do zapoznania się z witryną, dzięki której dowiesz się o początkach wspinania.
				</div>
						<div style="clear:both;"></div>
		</div>
			<div style="clear:both;"></div>
		<div id="naglowek2">
			<div id="sidebar">
				<div class="optionL"><a href="index.php?id=2" title="Wszystkie wiadomości o mnie" class="tilelink"> Trochę o mnie...</a> </div>
				<div class="optionL"><a href="index.php?id=3" title="Raczkowanie" class="tilelink"> Pierwsza przygoda ze wspinaniem</a></div>
				<div class="optionL"><a href="index.php?id=4" title="Coraz wyżej... i wyżej..." class="tilelink">Moje osiągnięcia</a></div>
				<div class="optionL"><a href="index.php?id=5" title="Moje zabawki" class="tilelink">Sprzęt</a> </div>
				<div class="optionL"><a href="index.php?id=6" title="Sweet focie" class="tilelink">Galeria </a> </div>
				<div class="optionL"><a href="index.php?id=7" title="Skały, skały, skałki" class="tilelink">Planowane wpiny skałkowe </a></div>
				<div style="clear:both;"></div>
			</div>
			
			<div id="content">
	
				
				<?php
				$id= @(int)$_GET["id"];
					if  (!$id>0) {
					$id=1;
				}
				$sql= "SELECT tresc FROM wspinaczka WHERE id=".$id;
				$result= mysql_query($sql) or die (mysql_error());
				$wynik = mysql_fetch_assoc($result);
				echo $wynik["tresc"];
				
				?>
				
			<p style="clear: left">
			</div>
				<div style="clear:both;"></div>
		</div>
			<div style="clear:both;"></div>
			<div class="socials">
			<div class="socialdivs">
				<div class="fb"> 
					<a href= "http://facebook.com/tosia06" target="_blank" class="sociallink">
					<i class=" icon-facebook-squared"></i></a>
				</div>
				<div class="yt">
					<a href= " https://www.youtube.com/channel/UCVFh3QoH848mzllL_iXlJfQ" target="_blank" class="sociallink">
					<i class=" icon-youtube-squared"></i></a>
				</div>
				<div class="gplus">
					<a href= "http://plus.google.com/u/0/100934172790342455085/posts" target="_blank" class="sociallink">
					<i class=" icon-gplus-squared"></i></a>
				</div>
				<div style="clear:both"></div>
			</div>
		</div>
	
		<div id="footer">
			Poznaj wspinaczkę. Najlepszy sport wytrzymałościowy. Strona w sieci od 2016r. &copy; Wszelkie prawa zastrzeżone.
		</div>
	
	</div>
	<script src="jquery-1.12.2.min.js"></script>
	<script>

		$(document).ready(function() {
		   var stickyNavTop = $('#menu').offset().top;

		   var stickyNav = function(){
		   var scrollTop = $(window).scrollTop();

		   if (scrollTop > stickyNavTop) { 
			  $('#menu').addClass('sticky');
			  $('#topbar').addClass('extramargin');
		   } else {
			  $('#menu').removeClass('sticky');
			   $('#topbar').removeClass('extramargin');
			}
		   };

		   stickyNav();

		   $(window).scroll(function() {
			  stickyNav();
		   });
		   });
	</script>




</body>
</html>
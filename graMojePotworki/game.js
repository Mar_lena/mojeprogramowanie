class Game {
	constructor(x,y,z){
		this.x = x;
		this.y = y;
		this.monstersNumber = z;
		this.monstersArray = [];
		this.bordPosition = [];
		this.numberRound = 0;
	}
	start (){
		if(this.monstersNumber > (this.x * this.y)){
			alert("Masz za dużo potworków");
			return false;
		}
		this.drawBoard(this.x,this.y);
		this.generateMonsters();
		this.generateStartingPosition();
	}
	playGame(){
		this.nextRound();
		var that = this;
		if(this.monstersArray.length < 2 ){

			//alert("Gra skończona! Wygrał monster " + this.monstersArray[0].getName() + " . Wygrana nastąpiła w: " + this.numberRound + " rundzie.");
            var finishMonster = this.monstersArray[0].getName();
            var finishRound = this.numberRound;
            $('.divBoard').hide("explode", {pieces: 30 }, 2000 );
			$( "#consol" ).prepend( "Gra skończona! Wygrał monster " + this.monstersArray[0].getName() + " . Wygrana nastąpiła w: " + this.numberRound + " rundzie. <br>");
            setTimeout(function(){alert("Gra skończona! Wygrał monster " + finishMonster + " . Wygrana nastąpiła w: " + finishRound + " rundzie.")},2000);

		} else {
			setTimeout(function(){that.playGame()}, 500);
		}
	}		
	nextRound(){
		this.numberRound++;
		for(var i = 0; i < this.monstersArray.length; i++){	
			if(this.monstersArray[i].getLive() <= 0){
				var removeName = this.monstersArray[i].getName();
				this.monstersArray[i].setPosition(null,null);
				this.monstersArray.splice(i,1);
				$( "#consol" ).prepend( "Niestety odpadł monster: "+ removeName + " . Odpadł w: " + this.numberRound + " rundzie. <br>");
			}
		}
		this.bordPosition = [];
		for(var i in this.monstersArray){
			this.monstersArray[i].moveMonster(this.x,this.y,this);
			var address = "x" + this.monstersArray[i].getPositionX() + "y" + this.monstersArray[i].getPositionY()
			if(this.bordPosition[address] == undefined ){
				this.bordPosition[address] = [];
			}
			this.bordPosition[address].push(this.monstersArray[i]);
		}
		for(var i in this.bordPosition){
			if(this.bordPosition[i].length === 2){
			
				this.fightMonsters(this.bordPosition[i][0],this.bordPosition[i][1]);
			}
		}
	}
	fightMonsters(monster1, monster2){
	$( "#consol" ).prepend("Walka pomiędzy: "+ monster1.getName() + " (życie: " + monster1.getLive() + " ) a " + monster2.getName() +  " (życie: " + monster2.getLive() + " ) " + " . Walka nastąpiła w: " + this.numberRound + " rundzie. <br>");
		if(monster1.getPower() > monster2.getPower()){
			monster1.setLive(monster1.getLive()-1);
			monster2.setLive(monster2.getLive()-2);
		} else if(monster1.getPower() < monster2.getPower()){
			monster1.setLive(monster1.getLive()-2);
			monster2.setLive(monster2.getLive()-1);
		} else {
			monster1.setLive(monster1.getLive()-1);
			monster2.setLive(monster2.getLive()-1);
		}
	}
	getAnotherMonsterFromCords(cordX,cordY,name){
		for(var i in this.monstersArray){
			if(this.monstersArray[i].getPositionX()===cordX && this.monstersArray[i].getPositionY()===cordY &&  this.monstersArray[i].getName()!==name ){
				return this.monstersArray[i];
			}
		} return false;
	}
	countMonstersFromCords(cordX, cordY){
		var numberOfMonsters = 0;
		for(var i in this.monstersArray){
			if(cordX === this.monstersArray[i].getPositionX() && cordY === this.monstersArray[i].getPositionY()){
				numberOfMonsters ++;
			}
		}
		return numberOfMonsters;	
	}
	generateMonsters(){
		for( var i = 0; i < this.monstersNumber; i++){
			var monster = new Monster('monster-' + i);
			this.monstersArray.push(monster);
		}
	}
	generateStartingPosition(){
		var monstersPositions = [];
		for(var i in this.monstersArray){
			do {
				var posX= Math.floor(Math.random()*this.x);
				var posY= Math.floor(Math.random()*this.y);
				var positions = 'x' + posX + 'y' + posY;
			} while(this.isPositionUnique(positions,monstersPositions) === false)
			monstersPositions.push(positions);
			this.monstersArray[i].setPosition(posX,posY);
		}
	}
	isPositionUnique(positions, monstersPositions){
		for( var j in monstersPositions){
			if(positions === monstersPositions[j]){
				return false;
			} 
		} 
		return true;
	}
	drawBoard(){
		$('.board').html(''); //czyści plansze
		for (var i = 0; i < this.x; i++){
			for (var j = 0; j < this.y; j++){
				$('<div/>', {
					style: 'float:left',
					class: 'divBoard',
					id: 'x' + i + 'y' + j,
				}).appendTo('.board') //wstawia pustego diva
			}
			$('<div/>', {
				style: 'clear:both', //przechodzi do kolejnego wiersza
				}).appendTo('.board');		
		}
		$('<div/>',{
			id: 'consol',
		}).appendTo('.board');
	}
}
class Monster {
	constructor(name){
		this.name = name;
		this.live = Math.floor(Math.random()*20)+1;
		this.power = Math.floor(Math.random()*5)+1;
		this.mobility = Math.floor(Math.random()*2)+2;
		this.look = this.generateLook();
		this.posX = 0;
		this.posY = 0;	
	}
	setPosition(posX,posY){
		this.posX = posX;
		this.posY = posY;
		$('#' + this.name).remove();
		$('#x' + posX + 'y' + posY).append('<img id="' + this.name + '" src="img/' + this.look + '">')
	}
	generateLook(){
		var monsterImages = ['m1.png', 'm2.png', 'm3.png', 'm4.png', 'm5.png', 'm6.png', 'm7.png', 'm8.png', 'm9.png', 'm10.png', 'm11.png', 'm12.png', 'm13.png', 'm14.png', 'm15.png'];
		return monsterImages[Math.floor(Math.random()*monsterImages.length)];
	}
	moveMonster(boardX, boardY,game){
		do{
			var newPositionX = this.posX + (Math.floor(Math.random()*this.mobility)) - (Math.floor(Math.random()*this.mobility));
			var newPositionY = this.posY + (Math.floor(Math.random()*this.mobility)) - (Math.floor(Math.random()*this.mobility));
			if(newPositionX >= boardX ){
				newPositionX = 0;
			} else if (newPositionX < 0){
				newPositionX = boardX-1;
			}
			if(newPositionY >= boardY ){
				newPositionY = 0;
			} else if (newPositionY < 0){
				newPositionY = boardY-1;
			}	
		}
		while(game.countMonstersFromCords(newPositionX,newPositionY) >= 2);
		this.setPosition(newPositionX, newPositionY);
		//console.log("liczba monsterow " + game.countMonstersFromCords(newPositionX,newPositionY));
		//console.log(this.name + "nowa pozycja x:" + newPositionX + "y:" + newPositionY);
	}
	getPositionX(){
		return this.posX;

	}
	getPositionY(){
		return this.posY;
	}
	getName(){
		return this.name;
	}
	getLive(){
		return this.live;
	}
	getPower(){
		return this.power;
	}
	setLive(newLiveMonster){
		this.live = newLiveMonster;
	}
}







